$(document).ready(function() {
    $('#tabla').DataTable2( {
        "language": {
            "lengthMenu":"Mostrar _MENU_ Procesos por página.",
             "zeroRecords": "Lo sentimos. No se encontraron registros.",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros aún.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "LoadingRecords": "Cargando ...",
            "Processing": "Procesando...",
             "SearchPlaceholder": "Comience a teclear...",
             "paginate": {
     "previous": "Anteior",
     "next": "Siguiente",
      }
        },


   "sort": false

    } );
} );
